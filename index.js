function doIt(){
    document.querySelector('.list-box').value = document.querySelector('.excel-box').value.split('\n').map(item => { return `'${item}'` }).join(', ');
}

function copyToClipboard(){
    const listBox = document.querySelector('.list-box');
    listBox.select();
    document.execCommand('copy');
}